package string.generators;

public class PersonMaxime {

    private String firstName;
    private String lastName;

    public PersonMaxime(){
        this("Maxime", "Esnol");
    }

    public PersonMaxime(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
